package com.mizarpvp.hcf;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.mizarpvp.hcf.commands.Broadcast;
import com.mizarpvp.hcf.commands.Day;
import com.mizarpvp.hcf.commands.Fly;
import com.mizarpvp.hcf.commands.Freeze;
import com.mizarpvp.hcf.commands.Gamemode;
import com.mizarpvp.hcf.commands.Hub;
import com.mizarpvp.hcf.commands.InFightEvents;
import com.mizarpvp.hcf.commands.Info;
import com.mizarpvp.hcf.commands.Kill;
import com.mizarpvp.hcf.commands.Msg;
import com.mizarpvp.hcf.commands.Nick;
import com.mizarpvp.hcf.commands.Night;
import com.mizarpvp.hcf.commands.Ping;
import com.mizarpvp.hcf.commands.Reply;
import com.mizarpvp.hcf.commands.Report;
import com.mizarpvp.hcf.commands.SetHealth;
import com.mizarpvp.hcf.commands.SetSpawn;
import com.mizarpvp.hcf.commands.Spawn;
import com.mizarpvp.hcf.commands.Speed;
import com.mizarpvp.hcf.commands.StaffChat;
import com.mizarpvp.hcf.commands.StaffMode;
import com.mizarpvp.hcf.commands.Store;
import com.mizarpvp.hcf.commands.Tp;
import com.mizarpvp.hcf.commands.TpHere;
import com.mizarpvp.hcf.commands.Ts;
import com.mizarpvp.hcf.commands.Vanish;
import com.mizarpvp.hcf.events.AntiVoid;
import com.mizarpvp.hcf.events.BowDamage;
import com.mizarpvp.hcf.events.Chat;
import com.mizarpvp.hcf.events.FreezeEvent;
import com.mizarpvp.hcf.events.JoinListener;
import com.mizarpvp.hcf.events.JoinQuitMessage;
import com.mizarpvp.hcf.events.MobSpawning;
import com.mizarpvp.hcf.utils.CColor;

public class Main extends JavaPlugin{

	public File file = new File(getDataFolder(), "settings.yml");
	public FileConfiguration data = YamlConfiguration.loadConfiguration(file);
	
	public ArrayList<Player> freeze = new ArrayList<>();
	public HashMap<String, String> reply = new HashMap<>();
	public ArrayList<Player> staffchat = new ArrayList<>();
	public ArrayList<Player> vanished = new ArrayList<>();
	public HashMap<String, ItemStack[]> itemSave = new HashMap<>();
	public HashMap<String, ItemStack[]> armorSave = new HashMap<>();
	public ArrayList<Player> staffmode = new ArrayList<>();
	public HashMap<Player, Integer> chatdelay = new HashMap<>();
	public HashMap<Player, Integer> reportdelay = new HashMap<>();
	List<String> announcer = getConfig().getStringList("announcer");
	List<String> commands = getConfig().getStringList("enable-commands");
	
	public void onEnable() {
		data.options().copyDefaults(true);
		try {
			data.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		getConfig().options().copyDefaults(true);
		saveConfig();
		Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		registerCommands();
		registerEvents();
		chatTimer();
		potionTimer();
		AnnouncerTimer();
		for(String cmds : commands) {
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmds);
		}
	}
	
	public void onDisable() {
		try {
			data.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void registerCommands() {
		getCommand("fly").setExecutor(new Fly(this));
		getCommand("gamemode").setExecutor(new Gamemode(this));
		getCommand("broadcast").setExecutor(new Broadcast(this));
		getCommand("freeze").setExecutor(new Freeze(this));
		getCommand("setspawn").setExecutor(new SetSpawn(this));
		getCommand("spawn").setExecutor(new Spawn(this));
		getCommand("tp").setExecutor(new Tp(this));
		getCommand("tphere").setExecutor(new TpHere(this));
		getCommand("day").setExecutor(new Day(this));
		getCommand("night").setExecutor(new Night(this));
		getCommand("msg").setExecutor(new Msg(this));
		getCommand("reply").setExecutor(new Reply(this));
		getCommand("staffchat").setExecutor(new StaffChat(this));
		getCommand("ping").setExecutor(new Ping(this));
		getCommand("vanish").setExecutor(new Vanish(this));
		getCommand("staffmode").setExecutor(new StaffMode(this));
		getCommand("info").setExecutor(new Info(this));
		getCommand("report").setExecutor(new Report(this));
		getCommand("speed").setExecutor(new Speed(this));
		getCommand("nick").setExecutor(new Nick(this));
		getCommand("ts").setExecutor(new Ts(this));
		getCommand("store").setExecutor(new Store(this));
		getCommand("kill").setExecutor(new Kill(this));
		getCommand("sethealth").setExecutor(new SetHealth(this));
		getCommand("hub").setExecutor(new Hub(this));
	}
	
	public void registerEvents() {
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new FreezeEvent(this), this);
		pm.registerEvents(new Chat(this), this);
		pm.registerEvents(new JoinQuitMessage(this), this);
		pm.registerEvents(new BowDamage(this), this);
		pm.registerEvents(new JoinListener(this), this);
		pm.registerEvents(new MobSpawning(this), this);
		pm.registerEvents(new AntiVoid(this), this);
		pm.registerEvents(new InFightEvents(this), this);
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void openFreezeGui(Player player){
      String name = CColor.CC("ScreenShare");
      String itemname = CColor.CC("&eYou have been frozen");
	  List itemlore = getConfig().getList("gui-item-lore");
      Inventory freezeGui = Bukkit.createInventory(null, 9, name);
      ItemStack Paper = new ItemStack(Material.PAPER);
      ItemMeta PaperMeta = Paper.getItemMeta();
      PaperMeta.setLore(itemlore);
      PaperMeta.setDisplayName(itemname);
      Paper.setItemMeta(PaperMeta);
      freezeGui.setItem(4, Paper);
      player.openInventory(freezeGui);
    }
	
	public void chatTimer() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @SuppressWarnings("deprecation")
			@Override
            public void run() {
            	
            for(Player all : Bukkit.getOnlinePlayers()) {
            if(all.getInventory().contains(new ItemStack(Material.GLASS_BOTTLE))) {
            				all.getInventory().remove(new ItemStack(Material.GLASS_BOTTLE));
            			}
            	
        	if(reportdelay.containsKey(all)) {
        		if(reportdelay.get(all) <= 0) {
        			reportdelay.remove(all);
        			return;
        		}
        		reportdelay.put(all, reportdelay.get(all) -1);
        	}
            
            	if(chatdelay.containsKey(all)) {
            		if(chatdelay.get(all) <= 0) {
            			chatdelay.remove(all);
            			return;
            		}
            		chatdelay.put(all, chatdelay.get(all) -1);
            	}
            }
            }
        }, 0, 20);
	}
	
	public void potionTimer() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @SuppressWarnings("deprecation")
			@Override
            public void run() {
            for(Player all : Bukkit.getOnlinePlayers()) {
            if(all.getInventory().contains(new ItemStack(Material.GLASS_BOTTLE))) {
            	all.getInventory().remove(new ItemStack(Material.GLASS_BOTTLE));
            	}
            }
            }
        }, 0, 10);
	}
	
	public void AnnouncerTimer() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @SuppressWarnings("deprecation")
			@Override
            public void run() {
            Random random = new Random();
            int messageid = random.nextInt(announcer.size());
            String message = announcer.get(messageid);
            for(Player all : Bukkit.getOnlinePlayers()) {
            	all.sendMessage(CColor.CC(message));
            }
            }
        }, 0, 10000);
	}
	
}
