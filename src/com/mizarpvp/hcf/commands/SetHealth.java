package com.mizarpvp.hcf.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mizarpvp.hcf.Main;
import com.mizarpvp.hcf.utils.CColor;

public class SetHealth implements CommandExecutor{

	Main plugin;
	
	public SetHealth(Main instance) {
		this.plugin = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String CommandLabel, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player) sender;
			if(player.hasPermission("mizarhcf.sethealth")) {
				if(args.length != 2) {
					player.sendMessage(CColor.CC("&cUsage: /sethealth <player> <value>"));
				}
				if(args.length == 2) {
					Player target = Bukkit.getPlayer(args[0]);
					if(target == null) {
						player.sendMessage(CColor.CC("&cPlayer not found."));
						return false;
					}
					try {
					double value = Double.parseDouble(args[1]);
					target.setHealth(value);
					player.sendMessage(CColor.CC("&e"+ target.getName() + "&6's health has been set to &e" + value));
					}catch(Exception ex) {
						player.sendMessage(CColor.CC("&cValue must be a valid value"));
					}
				}
			}else {
				player.sendMessage(CColor.CC("&cNo permissions."));
			}
		}else {
			sender.sendMessage(CColor.CC("&cYou aren't a player."));
		}
		return false;
	}
	
}
