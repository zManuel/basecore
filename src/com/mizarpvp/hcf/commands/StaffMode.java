package com.mizarpvp.hcf.commands;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.mizarpvp.hcf.Main;
import com.mizarpvp.hcf.utils.CColor;

public class StaffMode implements CommandExecutor{

	Main plugin;
	
	public StaffMode(Main instance) {
		this.plugin = instance;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String CommandLabel, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player) sender;
			if(player.hasPermission("mizarhcf.staffmode")) {
				if(plugin.staffmode.contains(player)) {
				plugin.staffmode.remove(player);
				player.getInventory().clear();
				player.getInventory().setArmorContents(null);
				if(this.plugin.itemSave.containsKey(player.getName())) {
					player.getInventory().setContents((ItemStack[]) this.plugin.itemSave.get(player.getName()));
				}
				if(plugin.armorSave.containsKey(player.getName())) {
					player.getInventory().setArmorContents((ItemStack[]) plugin.armorSave.get(player.getName()));
				}
			      for(Player all : Bukkit.getOnlinePlayers()) {
			    	  all.showPlayer(player);
			      }
			      plugin.vanished.remove(player);
			      plugin.itemSave.remove(player.getName());
			      plugin.armorSave.remove(player.getName());
			      player.setAllowFlight(false);
				player.sendMessage(CColor.CC("&6&lStaffMode &edisabled for &6&l" + player.getName()));
				return false;
				}else {
					plugin.staffmode.add(player);
				this.plugin.itemSave.put(player.getName(), player.getInventory().getContents());
				this.plugin.armorSave.put(player.getName(), player.getInventory().getArmorContents());
				plugin.vanished.add(player);
				player.getInventory().clear();
				player.getInventory().setArmorContents(null);
				
			      ItemStack Vanish = new ItemStack(Material.NETHER_STAR);
			      ItemMeta VanishMeta = Vanish.getItemMeta();
			      VanishMeta.setDisplayName("�2Vanish");
			      Vanish.setItemMeta(VanishMeta);
			      
			      ItemStack Freeze = new ItemStack(Material.PACKED_ICE);
			      ItemMeta FreezeMeta = Freeze.getItemMeta();
			      FreezeMeta.setDisplayName("�bFreeze");
			      Freeze.setItemMeta(FreezeMeta);
			      
			      ItemStack Ping = new ItemStack(Material.CHEST);
			      ItemMeta PingMeta = Ping.getItemMeta();
			      PingMeta.setDisplayName("�5Ping");
			      Ping.setItemMeta(PingMeta);
			      player.getInventory().setItem(2, Ping);
			      player.getInventory().setItem(5, Freeze);
			      player.getInventory().setItem(7, Vanish);
			      for(Player all : Bukkit.getOnlinePlayers()) {
			    	  all.hidePlayer(player);
			      }
			      player.setAllowFlight(true);
				player.sendMessage(CColor.CC("&6&lStaffMode &eenabled for &6&l" + player.getName()));
				return false;
				}
			}else {
				player.sendMessage(CColor.CC("&cNo permission"));
			}
		}else {
			sender.sendMessage(CColor.CC("&cYou aren't a player"));
		}
		return false;
	}
	
}
