package com.mizarpvp.hcf.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.mizarpvp.hcf.Main;
import com.mizarpvp.hcf.utils.CColor;

public class Store implements CommandExecutor{

	Main plugin;
	
	public Store(Main instance) {
		this.plugin = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String CommandLabel, String[] args) {
		sender.sendMessage(CColor.CC("&6Store&7� &estore.hakai.eu"));
		return false;
	}
	
}
