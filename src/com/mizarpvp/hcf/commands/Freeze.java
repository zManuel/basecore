package com.mizarpvp.hcf.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import com.mizarpvp.hcf.Main;
import com.mizarpvp.hcf.utils.CColor;

public class Freeze implements Listener, CommandExecutor{

	Main plugin;
	
	
	
	public Freeze(Main instance) {
		this.plugin = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String CommandLabel, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player) sender;
			if(player.hasPermission("mizarhcf.freeze")) {
				if(args.length == 0) {
					player.sendMessage(CColor.CC("&cUsage: /freeze <player>"));
				}
				
				if(args.length == 1) {
					Player target = Bukkit.getPlayer(args[0]);
					
					if(target == null) {
						player.sendMessage(CColor.CC("&cPlayer not online"));
					}else {
						if(target != player) {
							if(plugin.freeze.contains(target)) {
								plugin.freeze.remove(target);
								target.closeInventory();
								player.sendMessage(CColor.CC("&a" + target.getName() + " &7has been &aunfronzen"));
								target.sendMessage(CColor.CC("&aYou &7have been &aunfronzen"));
							}else {
						plugin.freeze.add(target);
						plugin.openFreezeGui(target);
						player.sendMessage(CColor.CC("&a" + target.getName() + " &7has been &afronzen"));
						target.sendMessage(CColor.CC("&aYou &7have been &bfronzen"));
						return false;
							}
						}else {
							player.sendMessage(CColor.CC("&cYou can't freeze yourself"));
						}
					}
				}
				
			}else {
				player.sendMessage(CColor.CC("&cNo permission"));
			}
		}else {
			sender.sendMessage(CColor.CC("&cYou must be a player"));
		}
		return false;
	}
	
	
	
}
