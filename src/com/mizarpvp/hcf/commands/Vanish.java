package com.mizarpvp.hcf.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mizarpvp.hcf.Main;
import com.mizarpvp.hcf.utils.CColor;

public class Vanish implements CommandExecutor{

	Main plugin;
	
	public Vanish(Main instance) {
		this.plugin = instance;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String CommandLabel, String[] args) {
		if(args.length == 0) {
			if(sender instanceof Player) {
				Player player = (Player) sender;
				if(plugin.vanished.contains(player)) {
				plugin.vanished.remove(player);
				for(Player all : Bukkit.getOnlinePlayers()) {
					all.showPlayer(player);
				}
				player.sendMessage(CColor.CC("&7Vanish &cdisabled &7for &6" + player.getName()));
				}else {
				plugin.vanished.add(player);
				for(Player all : Bukkit.getOnlinePlayers()) {
					all.hidePlayer(player);
				}
				player.sendMessage(CColor.CC("&7Vanish &aenabled &7for &6" + player.getName()));
				}
			}else {
				sender.sendMessage(CColor.CC("&cYou need to be a player!"));
			}
		}
		
		if(args.length == 1) {
			Player target = Bukkit.getPlayer(args[0]);
			if(plugin.vanished.contains(target)) {
				plugin.vanished.remove(target);
				for(Player all : Bukkit.getOnlinePlayers()) {
					all.showPlayer(target);
				}
				sender.sendMessage(CColor.CC("&7Vanish &cdisabled &7for &6" + target.getName()));
				target.sendMessage(CColor.CC("&7Vanish &cdisabled &7for &6" + target.getName()));
			}else {
				plugin.vanished.add(target);
				for(Player all : Bukkit.getOnlinePlayers()) {
					all.hidePlayer(target);
				}
				sender.sendMessage(CColor.CC("&7Vanish &aenabled &7for &6" + target.getName()));
				target.sendMessage(CColor.CC("&7Vanish &aenabled &7for &6" + target.getName()));
			}
		}
		
		return false;
	}
	
}
