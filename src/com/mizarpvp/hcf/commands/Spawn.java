package com.mizarpvp.hcf.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mizarpvp.hcf.Main;
import com.mizarpvp.hcf.utils.CColor;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class Spawn implements CommandExecutor{

	Main plugin;
	
	public Spawn(Main instance) {
		this.plugin = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String CommandLabel, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player) sender;
			World world = Bukkit.getWorld(plugin.data.getString("spawn.world"));
			int x = plugin.data.getInt("spawn.x");
			int y = plugin.data.getInt("spawn.y");
			int z = plugin.data.getInt("spawn.z");
			Location spawn = new Location(world, x, y, z);
		          for(ProtectedRegion r : WGBukkit.getRegionManager(player.getWorld()).getApplicableRegions(player.getLocation())){
		              if (r.getId().equalsIgnoreCase("kiteditor")) {
		            	  player.sendMessage(CColor.CC("&cYou are in kit editor!"));
		              }else {
		            	  player.teleport(spawn);
		              }
		          }
		}else {
			sender.sendMessage(CColor.CC("&cYou need to be a player"));
		}
		return false;
	}
	
	
	
}
