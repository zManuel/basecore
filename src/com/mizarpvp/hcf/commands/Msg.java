package com.mizarpvp.hcf.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mizarpvp.hcf.Main;
import com.mizarpvp.hcf.utils.CColor;

public class Msg implements CommandExecutor{

	Main plugin;
	
	public Msg(Main instance) {
		this.plugin = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String CommandLabel, String[] args) {
		
		if(sender instanceof Player) {
			if(args.length > 1) {
				Player player = (Player) sender;
				Player target = Bukkit.getPlayer(args[0]);
				if(target == null) {
					sender.sendMessage(CColor.CC("&cPlayer not found"));
				}else {
				      StringBuilder sb = new StringBuilder();
				      for (int i = 1; i < args.length; i++) {
				        sb.append(String.valueOf(args[i]) + " ");
				      }
				     String message = sb.toString();
				     player.sendMessage(CColor.CC("&e(To &6" + target.getName() + "&e) " + message));
				     target.sendMessage(CColor.CC("&e(From &6" + player.getName() + "&e) " + message));
				     plugin.reply.put(target.getName(), player.getName());
				}
				
			}else {
				sender.sendMessage(CColor.CC("Usage: /msg <player> <message>"));
				return false;
			}
		}else {
			Player target = Bukkit.getPlayer(args[0]);	
			if(target == null) {
				sender.sendMessage(CColor.CC("&cPlayer not found"));
			}else {
			      StringBuilder sb = new StringBuilder();
			      for (int i = 1; i < args.length; i++) {
			        sb.append(String.valueOf(args[i]) + " ");
			      }
			     String message = sb.toString();
			     sender.sendMessage(CColor.CC("&e(To &6Console&e)" + message));
			     target.sendMessage(CColor.CC("&e(From &6Console&e)" + message));
			}
		}
		
		return false;
	}
	
}
