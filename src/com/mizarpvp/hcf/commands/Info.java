package com.mizarpvp.hcf.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.mizarpvp.hcf.Main;
import com.mizarpvp.hcf.utils.CColor;

public class Info implements CommandExecutor{

	Main plugin;
	
	public Info(Main instance) {
		this.plugin = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String CommandLabel, String[] args) {
		sender.sendMessage(CColor.CC("&7&m-------------------------"));
		sender.sendMessage(CColor.CC("&6        &lHakai"));
		sender.sendMessage("");
		sender.sendMessage(CColor.CC("&eTs: &6ts.hakai.eu"));
		sender.sendMessage(CColor.CC("&eShop: &6store.hakai.eu"));
		sender.sendMessage(CColor.CC("&eWebSite: &6www.hakai.eu"));
		sender.sendMessage("");
		sender.sendMessage(CColor.CC("&6&l    hakai.eu"));
		sender.sendMessage(CColor.CC("&7&m-------------------------"));
		return false;
	}
	
}
