package com.mizarpvp.hcf.commands;

import java.io.IOException;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mizarpvp.hcf.Main;
import com.mizarpvp.hcf.utils.CColor;

public class SetSpawn implements CommandExecutor{

	Main plugin;
	
	public SetSpawn(Main instance) {
		this.plugin = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String CommandLabel, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player) sender;
			if(player.hasPermission("mizarhcf.setspawn")) {
			String world = player.getWorld().getName();
			int x = (int) player.getLocation().getX();
			int y = (int) player.getLocation().getY();
			int z = (int) player.getLocation().getZ();
			plugin.data.set("spawn.world", world);
			plugin.data.set("spawn.x", x);
			plugin.data.set("spawn.y", y);
			plugin.data.set("spawn.z", z);
			try {
				plugin.data.save(plugin.file);
			} catch (IOException e) {
				e.printStackTrace();
			}
			player.sendMessage(CColor.CC("&aSpawn set successfully!"));
			}else {
				player.sendMessage(CColor.CC("&cNo permissions."));
			}
		}else {
			sender.sendMessage(CColor.CC("&cYou need to be a player"));
		}
		return false;
	}
	
}
