package com.mizarpvp.hcf.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mizarpvp.hcf.Main;
import com.mizarpvp.hcf.utils.CColor;

public class StaffChat implements CommandExecutor{

	Main plugin;
	
	public StaffChat(Main instance) {
		this.plugin = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String CommandLabel, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player) sender;
			if(player.hasPermission("mizarhcf.staffchat")){
				if(plugin.staffchat.contains(player)) {
					plugin.staffchat.remove(player);
					player.sendMessage(CColor.CC("&6&lStaffChat&7>> &edisabled"));
					return false;
				}
				plugin.staffchat.add(player);
				player.sendMessage(CColor.CC("&6&lStaffChat&7>> &eenabled"));
				return false;
			}else {
				player.sendMessage(CColor.CC("&cNo permission"));
			}
		}else {
			sender.sendMessage(CColor.CC("&cYou need be a player!"));
		}
		return false;
	}
	
}
