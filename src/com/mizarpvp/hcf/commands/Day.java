package com.mizarpvp.hcf.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mizarpvp.hcf.Main;
import com.mizarpvp.hcf.utils.CColor;

public class Day implements CommandExecutor{

	Main plugin;
	
	public Day(Main instance) {
		this.plugin = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String CommandLabel, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player) sender;
			player.setPlayerTime(1200L, false);
			player.sendMessage(CColor.CC("&aTime set to day for you"));
		}else {
			sender.sendMessage(CColor.CC("&cYou aren't a player!"));
		}
		return false;
	}
	
}
