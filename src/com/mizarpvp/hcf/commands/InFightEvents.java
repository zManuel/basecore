package com.mizarpvp.hcf.commands;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

import com.mizarpvp.hcf.Main;

import ga.strikepractice.StrikePracticeAPI;

public class InFightEvents implements Listener{

	Main plugin;
	
	public InFightEvents(Main instance) {
		this.plugin = instance;
	}
	
	@EventHandler
	public void PlayerDropItemEvent (PlayerDropItemEvent e) {
		if(StrikePracticeAPI.isInFight(e.getPlayer())) {
			if(e.getItemDrop().getItemStack().getType() == Material.POTION || e.getItemDrop().getItemStack().getType() == Material.DIAMOND_SWORD || e.getItemDrop().getItemStack().getType() == Material.COOKED_BEEF || e.getItemDrop().getItemStack().getType() == Material.DIAMOND_HELMET || e.getItemDrop().getItemStack().getType() == Material.DIAMOND_CHESTPLATE || e.getItemDrop().getItemStack().getType() == Material.DIAMOND_LEGGINGS || e.getItemDrop().getItemStack().getType() == Material.DIAMOND_BOOTS ||  e.getItemDrop().getItemStack().getType() == Material.IRON_HELMET || e.getItemDrop().getItemStack().getType() == Material.IRON_CHESTPLATE || e.getItemDrop().getItemStack().getType() == Material.IRON_LEGGINGS || e.getItemDrop().getItemStack().getType() == Material.IRON_BOOTS || e.getItemDrop().getItemStack().getType() == Material.GOLD_HELMET || e.getItemDrop().getItemStack().getType() == Material.CHAINMAIL_LEGGINGS || e.getItemDrop().getItemStack().getType() == Material.LEATHER_BOOTS || e.getItemDrop().getItemStack().getType() == Material.LEATHER_CHESTPLATE || e.getItemDrop().getItemStack().getType() == Material.LEATHER_HELMET || e.getItemDrop().getItemStack().getType() == Material.LEATHER_LEGGINGS) {	
				return;
			}
			e.setCancelled(true);
			
		}
	}
	
}
