package com.mizarpvp.hcf.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mizarpvp.hcf.Main;
import com.mizarpvp.hcf.utils.CColor;

public class Report implements CommandExecutor{

	Main plugin;
	
	public Report(Main instance) {
		this.plugin = instance;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String CommandLabel, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player) sender;
			if(player.hasPermission("mizarhcf.report")) {
				if(args.length < 2) {
					player.sendMessage(CColor.CC("&cUsage: /report <player> <reason>"));
				}else {
					if(plugin.reportdelay.containsKey(player)) {
						player.sendMessage(CColor.CC("&cPlease wait " + plugin.reportdelay.get(player) + " seconds before report again."));
						return false;
					}
					Player target = Bukkit.getPlayer(args[0]);
					if(target == null) {
						player.sendMessage(CColor.CC("&cPlayer not online"));
					}else {
						if(target == player) {
							player.sendMessage(CColor.CC("&eYou can't report yourself."));
							return false;
						}
						StringBuilder buffer = new StringBuilder();
						for (int i = 1; i < args.length; i++) {
							buffer.append(' ').append(args[i]);
						}
						String reason = buffer.toString();
						plugin.reportdelay.put(player, 120);
						for(Player staffer : Bukkit.getOnlinePlayers()) {
							if(staffer.hasPermission("mizarhcf.report.notify")) {
								staffer.sendMessage("");
								staffer.sendMessage("�7�m*-------------------------------*�c");
								staffer.sendMessage("�7Reporter: �a" + player.getName());;
								staffer.sendMessage("�7Reported: �c" + target.getName());
								staffer.sendMessage("�7Reason: �c" + reason);
								staffer.sendMessage("�7�m*-------------------------------*�c");
								staffer.sendMessage("");	
							}
						}
						player.sendMessage(CColor.CC("&eReport subitted successfully!"));
					}
				}
			}else {
				player.sendMessage(CColor.CC("&cNo permission"));
			}
		}else {
			sender.sendMessage(CColor.CC("&cYou need be a player to execute this command"));
		}
		return false;
	}
	
}
