package com.mizarpvp.hcf.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;

import com.mizarpvp.hcf.Main;
import com.mizarpvp.hcf.utils.CColor;

import net.minecraft.server.v1_7_R4.EntityPlayer;

public class Ping implements CommandExecutor{

	Main plugin;
	
	public Ping(Main instance) {
		this.plugin = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String CommandLabel, String[] args) {
		
		if(sender instanceof Player) {
			Player player = (Player) sender;
			EntityPlayer ep = ((CraftPlayer)player).getHandle();
			
			if(args.length == 0) {
			player.sendMessage(CColor.CC("&7Your ping: " + ep.ping + "ms"));
			return false;
			}
			
			if(args.length == 1) {
			Player target = Bukkit.getPlayer(args[0]);
			if(target == null) {
				player.sendMessage(CColor.CC("&cPlayer not online"));
			}else {
				EntityPlayer ept = ((CraftPlayer)target).getHandle();
				player.sendMessage(CColor.CC("&7" + target.getName() + "'s ping: " + ept.ping + "ms"));
			}
			}
			
		}else {
			sender.sendMessage(CColor.CC("&cYou aren't a player!"));
		}
		
		return false;
	}
	
}
