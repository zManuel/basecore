package com.mizarpvp.hcf.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mizarpvp.hcf.Main;

public class Nick implements CommandExecutor{

	Main plugin;
	
	public Nick(Main instance) {
		this.plugin = instance;
	}

	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String CommandLabel, String[] args) {
	if(sender instanceof Player) {
		Player p = (Player) sender;
		if(p.hasPermission("mizarhcf.nick")) {
			if(args.length == 0) {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cUsage: /nick <nick>"));
			}
			if(args.length > 1) {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cNickname can't contain spaces"));
			}
			
			if(args.length == 1) {
				if(args[0].equalsIgnoreCase("off")) {
					p.setDisplayName(p.getName());
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aNickname removed!"));
				}else {
					p.setDisplayName(ChatColor.translateAlternateColorCodes('&', args[0]));
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aNickname applied!"));	
				}
			}
		}else {
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&fComando Sconosciuto."));
		}
	}else {
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "You are not a player!"));
	}
		return false;
	}
}