package com.mizarpvp.hcf.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mizarpvp.hcf.Main;
import com.mizarpvp.hcf.utils.CColor;

public class TpHere implements CommandExecutor{

	Main plugin;
	
	public TpHere(Main instance) {
		this.plugin = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String CommandLabel, String[] args) {
		if(sender instanceof Player) {
			
			Player player = (Player) sender;
			
			if(player.hasPermission("mizarhcf.tphere")) {
				if(args.length == 0) {
					player.sendMessage(CColor.CC("&cUsage: /tphere <player>"));
				}
				
				if(args.length == 1) {
					Player target = Bukkit.getPlayer(args[0]);
					if(target == null) {
						player.sendMessage(CColor.CC("&cPlayer not found"));
						return false;
					}
					target.teleport(player.getLocation());
					player.sendMessage(CColor.CC("&7Teleported &a" + target.getName() + "&7 to &ayou"));
				}
			}else {
				player.sendMessage(CColor.CC("&cNo permission"));
			}
		}else {
			sender.sendMessage(CColor.CC("&cYou need to be a player"));
		}
		return false;
	}
	
}
