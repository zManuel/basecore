package com.mizarpvp.hcf.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mizarpvp.hcf.Main;
import com.mizarpvp.hcf.utils.CColor;

public class Broadcast implements CommandExecutor{

	Main plugin;
	
	public Broadcast(Main instance) {
		this.plugin = instance;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String CommandLabel, String[] args) {
		if(sender.hasPermission("mizarhcf.broadcast")) {
			
			if(args.length == 0) {
				sender.sendMessage(CColor.CC("&cYou must supply a message"));
			}
			
    		@SuppressWarnings("unused")
			Player[] onlinePlayers;
    		if ((args.length >= 1) && ((onlinePlayers = Bukkit.getOnlinePlayers()).length != 0))
    	    {
    		      StringBuilder sb = new StringBuilder();
    		      for (int i = 0; i < args.length; i++) {
    		        sb.append(String.valueOf(args[i]) + " ");
    		      }
    		      
    		      String message = sb.toString();
    		      if(sender instanceof Player) {
    		    	  Player player = (Player) sender;
    		    	  player.playSound(player.getLocation(), Sound.NOTE_PLING, 1.0F, 3.0F);
    		      }
    		      Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&6&lHakai&7� " + message));
    	    }
			
		}else {
			sender.sendMessage(CColor.CC("&cNo permission"));
		}
		return false;
	}
	
}
