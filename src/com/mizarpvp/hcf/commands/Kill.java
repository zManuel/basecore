package com.mizarpvp.hcf.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mizarpvp.hcf.Main;
import com.mizarpvp.hcf.utils.CColor;

public class Kill implements CommandExecutor{

	Main plugin;
	
	public Kill(Main instance) {
		this.plugin = instance;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String CommandLabel, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player) sender;
			if(sender.hasPermission("mizarhcf.kill")) {
				if(args.length != 1) {
					player.sendMessage(CColor.CC("&cUsage: /kill <player>"));
				}
				if(args.length == 1) {
					Player target = Bukkit.getPlayer(args[0]);
					if(target == null) {
						player.sendMessage(CColor.CC("&cPlayer not found."));
						return false;
					}
					target.setHealth(0);
					player.sendMessage(CColor.CC("&6You killed &e" + target.getName()));
				}
			}else {
				player.sendMessage(CColor.CC("&cNo permission."));
			}
		}else {
			sender.sendMessage(CColor.CC("&cYou aren't a player."));
		}
		return false;
	}
	
}
