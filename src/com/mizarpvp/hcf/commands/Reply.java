package com.mizarpvp.hcf.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mizarpvp.hcf.Main;
import com.mizarpvp.hcf.utils.CColor;

import net.md_5.bungee.api.ChatColor;

public class Reply implements CommandExecutor{

	Main plugin;
	
	public Reply(Main instance) {
		this.plugin = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String CommandLabel, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player) sender;
			if(args.length > 0) {
				if (args.length >= 1) {
					if (plugin.reply.get(player.getName()) != null) {
					Player target = Bukkit.getPlayer(plugin.reply.get(player.getName()));
					if (target != null) {
						StringBuilder sb = new StringBuilder();
					      for (int i = 0; i < args.length; i++) {
					        sb.append(String.valueOf(args[i]) + " ");
					      }
					      
					      String message = sb.toString();
					      target.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e(From&6 " + player.getName() + "&e) " + message));
					      player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e(To&6 " + target.getName() + "&e) " + message));
					      plugin.reply.put(target.getName(), player.getName());
					}else {
						player.sendMessage("�cNo player has written to you");
					}
					}else {
						player.sendMessage("�cNo player has written to you");
					}
			}else {
				player.sendMessage(CColor.CC("Usage: /r <Message>"));
			}
		}else {
			player.sendMessage(CColor.CC("Usage: /r <player> <Message>"));
		}
	}else {
		sender.sendMessage(CColor.CC("&cYou are not a player!"));	
	}
		return false;
}
}
