package com.mizarpvp.hcf.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mizarpvp.hcf.Main;
import com.mizarpvp.hcf.utils.CColor;

public class Fly implements CommandExecutor{

	Main plugin;
	
	public Fly(Main instance) {
		this.plugin = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String CommandLabel, String[] args) {
		if(sender.hasPermission("mizarhcf.fly")) {
			if(args.length == 0) {
				if(sender instanceof Player) {
					Player player = (Player) sender;
					if(player.getAllowFlight()) {
						player.setAllowFlight(false);
						player.sendMessage(CColor.CC("&7Fly &cDisabled &7for &6" + player.getName()));
						return false;
					}else {
						player.setAllowFlight(true);
						player.sendMessage(CColor.CC("&7Fly &2Enabled &7for &6" + player.getName()));
						return false;
					}
				}else {
					sender.sendMessage(CColor.CC("&cYou are not a player"));
				}
			}
			
			if(args.length == 1) {
				
				Player target = Bukkit.getPlayer(args[0]);
				
				if(target == null) {
					sender.sendMessage(CColor.CC("&cPlayer not online"));
				}else {
					if(target.getAllowFlight()) {
					target.setAllowFlight(false);
					sender.sendMessage(CColor.CC("&7Fly &cDisabled &7for &6" + target.getName()));
					if(target != sender) {
					target.sendMessage(CColor.CC("&7Fly &cDisabled &7for &6" + target.getName()));
					}
					}else {
						target.setAllowFlight(true);
						sender.sendMessage(CColor.CC("&7Fly &2Enabled &7for &6" + target.getName()));
						if(target != sender) {
						target.sendMessage(CColor.CC("&7Fly &2Enabled &7for &6" + target.getName()));
						}
					}
				}
				
			}
			
		}else {
			sender.sendMessage(CColor.CC("&cNo permission"));
		}
		
		
		return false;
	}
	
}
