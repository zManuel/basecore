package com.mizarpvp.hcf.commands;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mizarpvp.hcf.Main;

public class Hub implements CommandExecutor{

	Main plugin;
	
	public Hub(Main instance) {
		this.plugin = instance;
	}

    public void teleport(Player pl, String input)
    {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);
        try {
            out.writeUTF("Connect");
            out.writeUTF(input);
        }
        catch (IOException localIOException) {}
        pl.sendPluginMessage(plugin, "BungeeCord", b.toByteArray());
    }
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String CommandLabel, String[] args) {
		
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "This command is only executable by players.");
            return true;
        }
        Player p = (Player)sender;
        teleport(p, "lobby");
        p.sendMessage(ChatColor.translateAlternateColorCodes('&',"&eYou have been sent to the &6&lHUB&e."));
        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
            public void run(){
                if(p.isOnline()){
                    p.sendMessage(ChatColor.translateAlternateColorCodes('&',"&cWe couldn't connect to the &4&lHUB&c, try again later."));
                }
            }
        }, 20 * 5);
        return true;
	}
	
}
