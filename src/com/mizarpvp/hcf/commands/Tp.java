package com.mizarpvp.hcf.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mizarpvp.hcf.Main;
import com.mizarpvp.hcf.utils.CColor;

public class Tp implements CommandExecutor{

	Main plugin;
	
	public Tp(Main instance) {
		this.plugin = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String CommandLabel, String[] args) {
		if(sender.hasPermission("mizarhcf.tp")) {
			if(args.length == 0) {
				sender.sendMessage(CColor.CC("&cUsage: /tp <player> [target]| /tp <x> <y> <z>"));
			}
			
			if(args.length == 1) {
				if(sender instanceof Player) {
				Player player = (Player) sender;
				Player target = Bukkit.getPlayer(args[0]);
				if(target == null) {
					sender.sendMessage(CColor.CC("&cPlayer not online"));
				}else {
					player.teleport(target.getLocation());
					player.sendMessage(CColor.CC("&aYou &7were teleported to &a" + target.getName()));
				}
			}else {
				sender.sendMessage(CColor.CC("&cYou are not a player"));
			}
			}
			
			if(args.length == 2) {
				
				Player player = Bukkit.getPlayer(args[0]);
				Player target = Bukkit.getPlayer(args[1]);
				if(player == null || target == null) {
					sender.sendMessage(CColor.CC("&cA player is not online"));
				}else {
					player.teleport(target.getLocation());
					sender.sendMessage(CColor.CC("&a" + player.getName() + " &7was teleported to " + target.getName()));
				}
				
			}
			
			if(args.length == 3){
				if(sender instanceof Player) {
				Player player = (Player) sender;
				if(player.hasPermission("mizarhcf.tp.others")) {
				try {
				int x = Integer.parseInt(args[0]);
				int y = Integer.parseInt(args[1]);
				int z = Integer.parseInt(args[2]);
				Location loc = new Location(player.getWorld(), x, y, z);
				player.teleport(loc);
				player.sendMessage(CColor.CC("&aYou &7were teleported to &a" + x + ", " + y + ", " + z));
				}catch(Exception ex) {
				player.sendMessage(CColor.CC("&cInvalid Cordinates"));
				}
				}else {
					player.sendMessage(CColor.CC("&cNo permissions to teleport other players to you."));
				}
			}else {
				sender.sendMessage(CColor.CC("&cYou aren't a player!"));
			}
			}
			
		}else {
			sender.sendMessage(CColor.CC("&cNo permission"));
		}
		return false;
	}
	
}
