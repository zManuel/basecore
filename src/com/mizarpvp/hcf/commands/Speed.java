package com.mizarpvp.hcf.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mizarpvp.hcf.Main;

public class Speed implements CommandExecutor{

	Main plugin;
	
	public Speed(Main instance) {
		this.plugin = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String CommandLabel, String[] args) {
    Player p = (Player) sender;
    if(sender instanceof Player) {
    	if(p.hasPermission("mizarhcf.speed")) {
    		if(args.length != 1) {
    			p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cUsage: /speed <value>"));
    		}
    		
    		if(args.length == 1) {
    			
    			if(!p.isFlying()) {
    				if(args[0].equalsIgnoreCase("0")) {
    					p.setWalkSpeed(0.0F);
    					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSet &cWalk &eSpeed to &c0"));
    				}
    				if(args[0].equalsIgnoreCase("1")) {
    					p.setWalkSpeed(0.1F);
    					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSet &cWalk &eSpeed to &c1"));
    				}
    				if(args[0].equalsIgnoreCase("2")) {
    					p.setWalkSpeed(0.2F);
    					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSet &cWalk &eSpeed to &c2"));
    				}
    				if(args[0].equalsIgnoreCase("3")) {
    					p.setWalkSpeed(0.3F);
    					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSet &cWalk &eSpeed to &c3"));
    				}
    				if(args[0].equalsIgnoreCase("4")) {
    					p.setWalkSpeed(0.4F);
    					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSet &cWalk &eSpeed to &c4"));
    				}
    				if(args[0].equalsIgnoreCase("5")) {
    					p.setWalkSpeed(0.5F);
    					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSet &cWalk &eSpeed to &c5"));
    				}
    				if(args[0].equalsIgnoreCase("6")) {
    					p.setWalkSpeed(0.6F);
    					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSet &cWalk &eSpeed to &c6"));
    				}
    				if(args[0].equalsIgnoreCase("7")) {
    					p.setWalkSpeed(0.7F);
    					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSet &cWalk &eSpeed to &c7"));
    				}
    				if(args[0].equalsIgnoreCase("8")) {
    					p.setWalkSpeed(0.8F);
    					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSet &cWalk &eSpeed to &c8"));
    				}
    				if(args[0].equalsIgnoreCase("9")) {
    					p.setWalkSpeed(0.9F);
    					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSet &cWalk &eSpeed to &c9"));
    				}
    				if(args[0].equalsIgnoreCase("10")) {
    					p.setWalkSpeed(1.0F);
    					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSet &cWalk &eSpeed to &c10"));
    				}
    				
    				if(!(args[0].equalsIgnoreCase("0") || (args[0].equalsIgnoreCase("1") || args[0].equalsIgnoreCase("2") || args[0].equalsIgnoreCase("3") || args[0].equalsIgnoreCase("4") || args[0].equalsIgnoreCase("5") || args[0].equalsIgnoreCase("6") || args[0].equalsIgnoreCase("7") || args[0].equalsIgnoreCase("8") || args[0].equalsIgnoreCase("9")|| args[0].equalsIgnoreCase("10")))) {
    					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cThe value must be from 0 to 10"));
    				}
    			}else {
    				if(args[0].equalsIgnoreCase("0")) {
    					p.setFlySpeed(0.0F);
    					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSet &cFly &eSpeed to &c0"));
    				}
    				if(args[0].equalsIgnoreCase("1")) {
    					p.setFlySpeed(0.1F);
    					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSet &cFly &eSpeed to &c1"));
    				}
    				if(args[0].equalsIgnoreCase("2")) {
    					p.setFlySpeed(0.2F);
    					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSet &cFly &eSpeed to &c2"));
    				}
    				if(args[0].equalsIgnoreCase("3")) {
    					p.setFlySpeed(0.3F);
    					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSet &cFly &eSpeed to &c3"));
    				}
    				if(args[0].equalsIgnoreCase("4")) {
    					p.setFlySpeed(0.4F);
    					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSet &cFly &eSpeed to &c4"));
    				}
    				if(args[0].equalsIgnoreCase("5")) {
    					p.setFlySpeed(0.5F);
    					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSet &cFly &eSpeed to &c5"));
    				}
    				if(args[0].equalsIgnoreCase("6")) {
    					p.setFlySpeed(0.6F);
    					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSet &cFly &eSpeed to &c6"));
    				}
    				if(args[0].equalsIgnoreCase("7")) {
    					p.setFlySpeed(0.7F);
    					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSet &cFly &eSpeed to &c7"));
    				}
    				if(args[0].equalsIgnoreCase("8")) {
    					p.setFlySpeed(0.8F);
    					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSet &cFly &eSpeed to &c8"));
    				}
    				if(args[0].equalsIgnoreCase("9")) {
    					p.setFlySpeed(0.9F);
    					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSet &cFly &eSpeed to &c9"));
    				}
    				if(args[0].equalsIgnoreCase("10")) {
    					p.setFlySpeed(1.0F);
    					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&eSet &cFly &eSpeed to &c10"));
    				}
    				
    				if(!(args[0].equalsIgnoreCase("0") || (args[0].equalsIgnoreCase("1") || args[0].equalsIgnoreCase("2") || args[0].equalsIgnoreCase("3") || args[0].equalsIgnoreCase("4") || args[0].equalsIgnoreCase("5") || args[0].equalsIgnoreCase("6") || args[0].equalsIgnoreCase("7") || args[0].equalsIgnoreCase("8") || args[0].equalsIgnoreCase("9")|| args[0].equalsIgnoreCase("10")))) {
    					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cThe value must be from 0 to 10"));
    				}
    			}
    		}
    	}else {
    	p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&fComando Sconosciuto."));	
    	}
    }else {
    	sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou are not a player"));
    }
		return false;
	}
	
}
