package com.mizarpvp.hcf.commands;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.mizarpvp.hcf.Main;
import com.mizarpvp.hcf.utils.CColor;

public class Gamemode implements CommandExecutor{

	Main plugin;
	
	public Gamemode(Main instance) {
		this.plugin = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String CommandLabel, String[] args) {
		if(sender.hasPermission("mizarhcf.gamemode")) {
			if(args.length == 0) {
				sender.sendMessage(CColor.CC("&cUsage: /gamemode <mode> [player]"));
			}
			
			if(args.length == 1) {
				if(sender instanceof Player) {
					Player player = (Player) sender;
					if(args[0].equalsIgnoreCase("0") || args[0].equalsIgnoreCase("survival") || args[0].equalsIgnoreCase("s")) {
						player.setGameMode(GameMode.SURVIVAL);
						player.sendMessage(CColor.CC("&7GameMode set to &cSURVIVAL &7for &6" + player.getName()));
						return false;
					}
					if(args[0].equalsIgnoreCase("1") || args[0].equalsIgnoreCase("creative") || args[0].equalsIgnoreCase("c")) {
						player.setGameMode(GameMode.CREATIVE);
						player.sendMessage(CColor.CC("&7GameMode set to &cCREATIVE &7for &6" + player.getName()));
						return false;
					}
					
					if(args[0].equalsIgnoreCase("2") || args[0].equalsIgnoreCase("adventure") || args[0].equalsIgnoreCase("a")) {
						player.setGameMode(GameMode.ADVENTURE);
						player.sendMessage(CColor.CC("&7GameMode set to &cADVENTURE &7for &6" + player.getName()));
						return false;
					}
				}else {
					sender.sendMessage(CColor.CC("&cYou are not a player"));
				}
			}
			
			if(args.length == 2) {
				
				Player target = Bukkit.getPlayer(args[1]);
				
				if(target == null) {
					sender.sendMessage(CColor.CC("&cPlayer not online"));
				}else {
					if(args[0].equalsIgnoreCase("0") || args[0].equalsIgnoreCase("survival") || args[0].equalsIgnoreCase("s")) {
						target.setGameMode(GameMode.SURVIVAL);
						target.sendMessage(CColor.CC("&7GameMode set to &cSURVIVAL &7for &6" + target.getName()));
						if(target != sender) {
						sender.sendMessage(CColor.CC("&7GameMode set to &cSURVIVAL &7for &6" + target.getName()));
						}
						return false;
					}
					if(args[0].equalsIgnoreCase("1") || args[0].equalsIgnoreCase("creative") || args[0].equalsIgnoreCase("c")) {
						target.setGameMode(GameMode.CREATIVE);
						target.sendMessage(CColor.CC("&7GameMode set to &cCREATIVE &7for &6" + target.getName()));
						if(target != sender) {
						sender.sendMessage(CColor.CC("&7GameMode set to &cCREATIVE &7for &6" + target.getName()));
						}
						return false;
					}
					
					if(args[0].equalsIgnoreCase("2") || args[0].equalsIgnoreCase("adventure") || args[0].equalsIgnoreCase("a")) {
						target.setGameMode(GameMode.ADVENTURE);
						target.sendMessage(CColor.CC("&7GameMode set to &cADVENTURE &7for &6" + target.getName()));
						if(target != sender) {
						sender.sendMessage(CColor.CC("&7GameMode set to &cADVENTURE &7for &6" + target.getName()));
						}
						return false;
					}	
				}
				
			}
			
		}else {
			sender.sendMessage(CColor.CC("&cNo permission"));
		}
		return false;
	}
	
	
	
}
