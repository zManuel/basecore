package com.mizarpvp.hcf.events;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import com.mizarpvp.hcf.Main;
import com.mizarpvp.hcf.utils.CColor;

public class BowDamage implements Listener{

	Main plugin;
	
	public BowDamage(Main instance) {
		this.plugin = instance;
	}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onBowHit(EntityDamageByEntityEvent e) {
		Entity entity = e.getEntity();
		if(entity instanceof Player) {
			Player player = (Player) entity;
		if(e.getDamager() instanceof Arrow) {
			Arrow arrow = (Arrow) e.getDamager();
			if(arrow.getShooter() instanceof Player) {
				Player shooter = (Player) arrow.getShooter();
				double healthd = ((Damageable)player).getHealth() - e.getFinalDamage();
				double health = (Math.floor(healthd * 10) / 10);
				if(health > 0) {
				shooter.sendMessage(CColor.CC("&c" + player.getName() + "&7  is now at &c" + health + "&4❤"));
				}
			}
		}
	}
	}
	
}
