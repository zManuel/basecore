package com.mizarpvp.hcf.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.mizarpvp.hcf.Main;

public class JoinListener implements Listener{

	Main plugin;
	
	public JoinListener(Main instance) {
		this.plugin = instance;
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		for(Player vanished : plugin.vanished) {
		e.getPlayer().hidePlayer(vanished);
		}
	}
	
}
