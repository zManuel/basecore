package com.mizarpvp.hcf.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import com.mizarpvp.hcf.Main;
import com.mizarpvp.hcf.utils.CColor;

public class JoinQuitMessage implements Listener{

	Main plugin;
	
	public JoinQuitMessage(Main instance) {
		this.plugin = instance;
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		e.setJoinMessage(CColor.CC("&7[&2+&7] " + e.getPlayer().getName()));
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		if(plugin.vanished.contains(e.getPlayer())) {
		plugin.vanished.remove(e.getPlayer());
		}
		if(plugin.staffmode.contains(e.getPlayer())) {
			e.getPlayer().getInventory().clear();
			e.getPlayer().getInventory().setArmorContents(null);
			if(this.plugin.itemSave.containsKey(e.getPlayer().getName())) {
				e.getPlayer().getInventory().setContents((ItemStack[]) this.plugin.itemSave.get(e.getPlayer().getName()));
			}
			if(plugin.armorSave.containsKey(e.getPlayer().getName())) {
				e.getPlayer().getInventory().setArmorContents((ItemStack[]) plugin.armorSave.get(e.getPlayer().getName()));
			}
		      for(Player all : Bukkit.getOnlinePlayers()) {
		    	  all.showPlayer(e.getPlayer());
		      }
				plugin.staffmode.remove(e.getPlayer());

		}
		e.setQuitMessage(CColor.CC("&7[&4-&7] " + e.getPlayer().getName()));
	}
	
}
