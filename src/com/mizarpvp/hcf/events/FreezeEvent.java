package com.mizarpvp.hcf.events;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import com.mizarpvp.hcf.Main;
import com.mizarpvp.hcf.utils.CColor;

public class FreezeEvent implements Listener{

	Main plugin;
	
	public FreezeEvent(Main instance) {
		this.plugin = instance;
	}
	
	@EventHandler
	public void InventoryClickEvent(InventoryClickEvent e) {
		HumanEntity player = e.getWhoClicked();
		if(player instanceof Player) {
			Player p = (Player) player;
			
			if(plugin.freeze.contains(p)) {
				e.setCancelled(true);
			}
			
		}
	}
	
	@EventHandler
	public void onTakeDamage(EntityDamageEvent e) {
		if(e.getEntity() instanceof Player) {
			Player player = (Player) e.getEntity();
			if(plugin.freeze.contains(player) || plugin.staffmode.contains(player)) {
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onPlayerAttack(EntityDamageByEntityEvent e) {
		if(e.getEntity() instanceof Player && e.getDamager() instanceof Player) {
			Player p = (Player) e.getDamager();
			if(plugin.freeze.contains(p) || plugin.staffmode.contains(p)) {
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		if(plugin.freeze.contains(p)) {
			plugin.freeze.remove(p);
			if(plugin.getConfig().getBoolean("freeze-ban-onquit")) {
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), plugin.getConfig().getString("freeze-ban-command").replaceAll("%player%", p.getName()));
				Bukkit.broadcastMessage(CColor.CC("&4" + p.getName() + " &clogged out while frozen"));
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onInventoryClose(InventoryCloseEvent e) {
		HumanEntity player = e.getPlayer();
		boolean isPlayer = player instanceof Player;
		if (isPlayer) {
			Player p = (Player) player;
			if (plugin.freeze.contains(p)) {
				Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new BukkitRunnable() {

					@Override
					public void run() {
						plugin.openFreezeGui(p);
						
					}
					
				}, 1L);
			}
		}
		
	}
	
	@EventHandler
	public void onBreak(BlockBreakEvent e) {
		if(plugin.freeze.contains(e.getPlayer()) || plugin.staffmode.contains(e.getPlayer())) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlace(BlockPlaceEvent e) {
		if(plugin.freeze.contains(e.getPlayer()) || plugin.staffmode.contains(e.getPlayer())) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent e) {
		Player p = e.getPlayer();
		if (plugin.staffmode.contains(p)) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPickUp(PlayerPickupItemEvent e) {
		Player p = e.getPlayer();
		if (plugin.staffmode.contains(p)) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent e) {
		Entity entity = e.getEntity();
		boolean isPlayer = entity instanceof Player;
		if (isPlayer) {
			Player p = (Player) entity;
			if (plugin.staffmode.contains(p) || plugin.freeze.contains(p)) {
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onClickEntity(PlayerInteractEntityEvent e) {
		Entity entity = e.getRightClicked();
		Player player = e.getPlayer();
		if(entity instanceof Player && plugin.staffmode.contains(player)) {
			if (player.getItemInHand().getItemMeta().getDisplayName().equals("�5Ping")) {
				Player target = (Player) entity;
				if(target != null) {
					int ping = ((CraftPlayer) target).getHandle().ping;
					player.sendMessage(CColor.CC("&7" + target.getName() + "'s ping: " + ping + "ms"));
				}
			}else {
				if (player.getItemInHand().getItemMeta().getDisplayName().equals("�bFreeze")) {
					Player target = (Player) entity;
					player.performCommand("freeze " + target.getName());
				}
			}
		}
	}
	
	@EventHandler
	public void onPlayerClick(PlayerInteractEvent e) {
		Player player = e.getPlayer();
		if(plugin.staffmode.contains(player)) {
			if(e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK) {
				return;
			}
				switch(player.getItemInHand().getItemMeta().getDisplayName()) {
				case "�2Vanish":
					player.performCommand("vanish");
					e.setCancelled(true);
					break;
					
				default:
					break;
			}
		}
	}
	
}
