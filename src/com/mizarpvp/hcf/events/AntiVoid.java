package com.mizarpvp.hcf.events;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import com.mizarpvp.hcf.Main;

public class AntiVoid implements Listener{

	Main plugin;
	
	public AntiVoid(Main instance) {
		this.plugin = instance;
	}
	
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		if(e.getPlayer().getLocation().getWorld() == Bukkit.getWorld("world")) {
			if(e.getPlayer().getLocation().getY() <= 3) {
				Player player = e.getPlayer();
				World world = Bukkit.getWorld(plugin.data.getString("spawn.world"));
				int x = plugin.data.getInt("spawn.x");
				int y = plugin.data.getInt("spawn.y");
				int z = plugin.data.getInt("spawn.z");
				Location spawn = new Location(world, x, y, z);
				player.teleport(spawn);
			}
		}
	}
	
}
