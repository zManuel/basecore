package com.mizarpvp.hcf.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.mizarpvp.hcf.Main;
import com.mizarpvp.hcf.utils.CColor;

import ru.tehkode.permissions.bukkit.PermissionsEx;

public class Chat implements Listener{

	Main plugin;
	
	public Chat(Main instance) {
		this.plugin = instance;
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		Player player = e.getPlayer();
		if(plugin.staffchat.contains(player)) {
			for(Player staffer : Bukkit.getOnlinePlayers()) {
				if(staffer.hasPermission("mizarhcf.staffchat")) {
					e.setCancelled(true);
					staffer.sendMessage(CColor.CC("&5StaffChat&7>> &b" + player.getName() + " &7: &b" + e.getMessage()));
				}
			}
		}else {
		String prefix = "";
		try {
		String a = PermissionsEx.getUser(e.getPlayer()).getGroups()[0].getPrefix();
		prefix = a;
		}catch(Exception ex){
			prefix = "";
		}
		String message = e.getMessage();
		
		String format = CColor.CC(prefix + player.getDisplayName() + "&f: ");
		if(player.hasPermission("mizarhcf.chatcolor")) {
			message = CColor.CC(message);
		}
		if(plugin.chatdelay.containsKey(player)) {
			e.setCancelled(true);
			player.sendMessage(CColor.CC("&6Please wait before sending another messag or buy a vip on store.hakai.eu"));
		}else {
		e.setFormat(format + message);
		if(!player.hasPermission("mizarhcf.bypasschatdelay")) {
		plugin.chatdelay.put(player, 2);
		}
		}
	}
	}
	
}
